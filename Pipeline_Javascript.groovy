pipeline {
    agent any

    stages {
        stage('Checkout') {
            steps {
                // Checkout code from the GitHub repository
                git 'https://github.com/tonykhbo/hello-world-nextjs.git'
            }
        }

        stage('Build and Run') {
            steps {
                // Build and run the JavaScript application
                sh '''
                    docker build . -f Dockerfile -t hello-world-nextjs .
                    // docker push "nama containter registery"
                '''
            }
        }

        stage('Check IP and Port') {
            steps {
                // Execute a shell script to check the IP and Port
                sh '''
                    IP=$(docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' my-app)
                    PORT=8080
                    echo "IP: $IP, Port: $PORT"
                '''
            }
        }
    }
}
