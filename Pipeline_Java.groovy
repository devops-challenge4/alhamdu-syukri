pipeline {
    agent any

    stages {
        stage('Checkout') {
            steps {
                // Checkout code from the GitHub repository
                git 'https://github.com/BuntyRaghani/spring-boot-hello-world.git'
            }
        }

        stage('Build and Run') {
            steps {
                // Build the Java application using Maven
                sh 'mvn clean install'

                // Build and run the Docker container
                sh '''
                    docker build -t spring-boot-hello-world .
                    docker run -d -p 8080:8080 --name my-app spring-boot-hello-world
                '''
            }
        }

        stage('Check IP and Port') {
            steps {
                // Execute a shell script to check the IP and Port
                script {
                    def ip = sh(script: "docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' my-app", returnStatus: true).trim()
                    def port = 8080
                    echo "IP: ${ip}, Port: ${port}"
                }
            }
        }
    }
}
